<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Form\CategoryType;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog_list")
     */
    public function list(ArticleRepository $repo, EntityManagerInterface $em, PaginatorInterface $paginator, Request $request) 
    {
        $query = $repo->getArticles();

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        $pagination->setCustomParameters([
            'position' => 'centered',
            'size' => 'large',
            'rounded' => true,
        ]);
        
        return $this->render('blog/list.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/article/new", name="article_add")
     */
    public function addArticle(Request $request): Response
    {
        $article = new Article();
        // documentation : https://symfony.com/doc/current/forms.html
        $form = $this->createForm(ArticleType::class, $article);

        // Gestion du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return new Response('Le formulaire a été soumis');
        }

        return $this->render("blog/article_add.html.twig", [
            'form' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * @Route("article/edit/{id}", name="article_edit", requirements={"id"="\d+"})
     */
    public function editArticle(Article $article, Request $request)
    {
        dump($article);
        if (is_null($article)) {
            return new Response("L'article n'existe pas");
        }

        $form = $this->createForm(ArticleType::class, $article);
        // Gestion du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('blog_list');
        }

        return $this->render("blog/article_add.html.twig", [
            'form' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * @Route("/category", name="category_index")
     */
    public function listCategory(CategoryRepository $repo)
    {
        // findAll nous ramène l'ensemble des catégories en base
        $categories = $repo->findAll();
        return $this->render('blog/category_list.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/new", name="category_add")
     */
    public function addCategory(Request $request): Response
    {
        $category = new Category();
        // documentation : https://symfony.com/doc/current/forms.html
        $form = $this->createForm(CategoryType::class, $category);

        // Gestion du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return new Response('Le formulaire a été soumis');
        }

        return $this->render("blog/category_add.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("category/edit/{id}", name="category_edit", requirements={"id"="\d+"})
     */
    public function editCategory(int $id, CategoryRepository $repo, Request $request)
    {
        $category = $repo->findById($id);

        if (is_null($category)) {
            return new Response("La catégorie n'existe pas");
        }

        $form = $this->createForm(CategoryType::class, $category);

        return $this->render("blog/category_add.html.twig", [
            'form' => $form->createView()
        ]);
    }
}