<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Notification\ContactNotification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     * Cette fonction répond à l'url "/contact" et
     */
    public function index(Request $request, ContactNotification $notification)
    {
        // Création d'un nouveau contact
        $contact = new Contact();
        // Création du formulaire ContactType pour l'objet contact
        $form = $this->createForm(ContactType::class, $contact);

        // Gestion de la requête par le formulaire
        $form->handleRequest($request);
        // Validation du formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            $notification->notify($contact);
            $this->addFlash('success', 'Votre email a bien été envoyé, nous vous répondrons dans les meilleurs délais');
            return $this->redirectToRoute('contact');
        }
        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
