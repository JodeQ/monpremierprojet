<?php
namespace App\Notification;

use App\Entity\Contact;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class ContactNotification {

  /**
   * @var MailerInterface
   */
  private $mailer;

  public function __construct(MailerInterface $mailer)
  {
    $this->mailer = $mailer;
  }
  
  /**
   * notify envoi un email avec les informations de l'entité Contact
   *
   * @param  Contact $contact
   * @return void
   */
  public function notify(Contact $contact)
  {
    $email = (new TemplatedEmail())
      ->from($contact->getEmail())
      ->to(new Address('admin@monsite.fr'))
      ->subject('Nouvelle demande de ' . $contact->getLastname())
      ->htmlTemplate('emails/contact.html.twig')
      ->context([
        'contact' => $contact,
      ]);

    $this->mailer->send($email);
  }

}