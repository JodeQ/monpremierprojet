<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 5; $i++)
        {
            // Création d'un nouvelle catégorie
            $category = new Category();
            // Label aléatoire avec 20 caractères maximum
            $category->setLabel($faker->text(20));
            // Sauvegarde de la catégorie
            $manager->persist($category);

            $nb_article = random_int(4, 8);
            for ($j = 0; $j < $nb_article; $j++) {
                $article = new Article();
                $article
                    ->setTitle($i . "-" . $j . " : " . $faker->text(20))
                    ->setContent($faker->paragraphs(5, true))
                    ->setPicture("https://via.placeholder.com/150x75.png?text=" . htmlentities($faker->text(15)))
                    ->addCategory($category)
                ;
                // Sauvegarde de l'article
                $manager->persist($article);
            }
        }
        // Enregistrement en base de donnée
        $manager->flush();
    }
}
