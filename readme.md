# Projet Symfony

Ceci est un projet d'exemple pour l'utilisation de Symfony 5

Ce fichier readme permet de lister les commandes utilisées pour créer ce projet

## Pré-requis pour Symfony 5

Créer un nouveau projet nécessite l'installation du client symfony cli

cli signifie : command line interface, il s'agit de la ligne de commande à utiliser dans un terminal (cmd sous windows, Terminal sous mac, ...)

le processus d'installation est disponible sur le site de symfony [ici](https://symfony.com/doc/current/setup.html "Installing & Setting up the Symfony Framework")

Les pré-requis sont donc

 - Avoir un serveur PHP version 7.2.5 minimum avec les extensions
   -  Ctype
   -  iconv
   -  JSON
   -  PCRE
   -  Session
   -  SimpleXML
   -  Tokenizer
 - installer [composer](https://getcomposer.org/download/)
 - Installer le client [symfony](https://symfony.com/download)

>wget https://get.symfony.com/cli/installer -O - | bash

## Créer un nouveau projet

Maintenant que symfony cli est installé vous avez une nouvelle commande dans votre terminal `symfony`

En lancant la commande suivante vous voyez les différentes options disponibles

```
symfony
```
```
Symfony CLI version v4.12.10 (c) 2017-2020 Symfony SAS
Symfony CLI helps developers manage projects, from local code to remote infrastructure

These are common commands used in various situations:

Work on a project locally

  new                                                            Create a new Symfony project
  serve                                                          Run a local web server
  server:stop                                                    Stop the local web server
  security:check                                                 Check security issues in project dependencies
  composer                                                       Runs Composer without memory limit
  console                                                        Runs the Symfony Console (bin/console) for current project
  php, pecl, pear, php-fpm, php-cgi, php-config, phpdbg, phpize  Runs the named binary using the configured PHP version

Manage a project on Cloud

  login        Log in with your SymfonyConnect account
  init         Initialize a new project using templates
  link         Link current git repository to a SymfonyCloud project
  projects     List active projects
  envs         List environments
  env:create   Create an environment
  tunnel:open  Open SSH tunnels to the app's services
  ssh          Open an SSH connection to the app container
  deploy       Deploy an environment
  domains      List domains
  vars         List variables
  user:add     Add a user to the project

Show all commands with symfony help,
Get help for a specific command with symfony help COMMAND.
```




# Créer un formulaire de contact

## Création d'une nouvelle Entité manuellement (car non liée à la base de donnée)

Dans le dossier src/Entity on créé le fichier Contact.php

Le fichier Contact.php fera donc parti du namespace App\Entity.
`namespace App\Entity;`

Nous lui ajoutons également de propriétés :
 - firstname
 - lastname
 - phone
 - email
 - message

Avec l'extension vscode `PHP Getters & Setters` vous pouvez facilement créé les getters et setters de vos propriétés enfaisant un clic droit sur la propriété

## Création du formaulaire de contact

Avec la commande `symfony console make:form` créons le formulaire `ContactType` qui n'est basé sur aucune entité.

le fichier sera créé dans `src/Form/ContactType.php`

Editons son contenu pour créer un formulaire de nos champs Contact.

Dans la fonction buildForm nous modifions l'appel de $builder ainsi :
```
$builder
    ->add('firstname', TextType::class)
    ->add('lastname', TextType::class)
    ->add('phone', TextType::class)
    ->add('email', TextType::class)
    ->add('message', TextareaType::class)
;
```

En utilisant l'autocomplétion de l'extension `Symfony for VSCode` les différentes classes seront incluses dans notre classe ContactType.

Ce formulaire va nous servir dans le contrôleur que nous allons créer pour l'afficher et le gérer.

## Création du conteneur de contact

Avec la commande `symfony console make:controller` nous créons le contrôleur ContactController.

Les fichiers `src/Controller/ContactController.php` et `templates/contact/index.html.twig` seront crées.

Dans le fichier `ContactController` ajoutons l'affichage du formulaire.

Par défaut la fonction `index` pour la route `/contact` a été crée. Au début de cette fonction nous ajoutons le code suivant :
```
// Création d'un nouveau contact
$contact = new Contact();
// Création du formulaire ContactType pour l'objet contact
$form = $this->createForm(ContactType::class, $contact);
```

puis dans l'appel à la fonction de rendu `$this->render` nous modifions l'appel par celui-ci :
```
return $this->render('contact/index.html.twig', [
    'form' => $form->createView(),
]);
```

Maintenant pour permettre l'affichage de ce formulaire nous devons modifier le modèle twig qui est appelé : `contact/index.html.twig` que l'on peut trouver là : `templates/contact/index.html.twig`

Par défaut son contenu étend le template de base `base.html.twig`, ce qui permet d'avoir un code html structuré dans notre application. Dans ce template nous allons écraser le block `{% block body %}` avec ce contenu :
```
{% block body %}
<div class="container">
    <div id="contactForm" class="mt-4">
        {{ form_start(form) }}
            <div class="row">
                <div class="col">{{ form_row(form.firstname) }}</div>
                <div class="col">{{ form_row(form.lastname) }}</div>
            </div>
            <div class="row">
                <div class="col">{{ form_row(form.email) }}</div>
                <div class="col">{{ form_row(form.phone) }}</div>
            </div>
            {{ form_rest(form) }}
            <div class="form-group">
                <button class="btn btn-primary">Envoyer</button>
            </div>
        {{ form_end(form) }}
    </div>
</div>
{% endblock %}
```

Vous remarquerez l'utilisation de classes bootstrap, pour nous en servir rapidement nous modifions également le fichier `templates/base.html.twig` en ajoutant la ligne `<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">` dans le bloc html `<head>`.

Maintenant nous pouvons afficher notre formulaire.

Démarrons le serveur symfony avec la commande `symfony serve` et allons à l'url fournie dans la commande en ajoutant `/contact` à la fin.

Pour moi il s'agit de l'url [`http://127.0.0.1:8000/contact`](http://127.0.0.1:8000/contact).

## Récupération des données du formulaire
Lorsque le bonton `Envoyer` est cliqué le formulaire est soumis mais pour l'instant rien n'est traité.
Nous modifions le `ContactController` pour faire cela.

Première chose, nous ajoutons le paramètre `$request` de type `Request` dans la fonction index
```
public function index(Request $request)
```
Ainsi nous avons, grâçe à l'injection de dépendance Symfony, accès aux informations de la requête.

Après la déclaration du formulaire et avant le rendu nous ajoutons le gestion de cette requête avec la ligne suivante
```
// Gestion de la requête par le formulaire
$form->handleRequest($request);
// Validation du formulaire
if ($form->isSubmitted() && $form->isValid()) {
    $this->addFlash('success', 'Votre email a bien été envoyé, nous vous répondrons dans les meilleurs délais');
    return $this->redirectToRoute('contact');
}
```

Ce bloc de code envoi la requête et formaulaire puis teste si :
 - le formulaire a été soumi
 - le formulaire est valide

Si ces deux conditions sont valides alors nous traitons les données envoyées, pour l'instant nous ajoutons un message flash et nous nous redirigeons à nouveau vers le formulaire vide.

Pour afficher les messages flash nous devons modifier le modèle de base avec ce bloc au dbut du bloc html `<body>`
```
{# read and display several types of flash messages #}
{% for label, messages in app.flashes(['success', 'warning']) %}
    {% for message in messages %}
        <div class="alert alert-{{ label }}">
            {{ message }}
        </div>
    {% endfor %}
{% endfor %}
```

Puis il faut maintenant nous envoyer un email avec les informations saisies.

## Configurer le mailer symfony

Pour structurer le code nous allons créer une nouvelle classe pour gérer les notifications. Nous créons donc une classe `ContactNotification` dans le nouveau namespace `App\Notification`.

Nous créons donc le dossier `src/Notification` puis, dans ce dossier, le fichier `ContactNotification.php` avec le contenu suivant
```
<?php
namespace App\Notification;

use App\Entity\Contact;

class ContactNotification {

  public function notify(Contact $contact) {
    
  }

}
```

Puis dans le `ContactController` nous ajoutons en tant que dépendance d'injection cette nouvelle classe
```
public function index(Request $request, ContactNotification $notification)
```
Et nous l'utilisons au dessus de l'appel addFlash de confirmation avec l'appel :
```
$notification->notify($contact);
```
Comme cela toute la gestion de l'envoi du mail sera traité dans cette classe dédié et facilement testable.

Afin de pouvoir tester plus facilement je vous propose de commenter temporairement le bloc de redirection en dessous pour conserver les données dans le formulaire.
```
// return $this->redirectToRoute('contact');
```

## Configuration du mailer Symfony

La documentation se trouve [ici](https://symfony.com/doc/current/mailer.html).

Pour simplifier le développement nous allons utiliser [maildev](https://www.npmjs.com/package/maildev) pour ne pas spammer inutilement notre boite mail.

```
npm install -g maildev
ou pour les utilisateurs de Mac
sudo npm install -g maildev
```

puis dans le fichier `.env` nous pouvons ajouter la ligne suivante
```
MAILER_DSN=smtp://127.0.0.1:1025
```

Dans un terminal nous démarrons maildev
```
maildev --hide-extensions STARTTLS
```
Et nous savons maintenant qu'il utilise le port SMTP 1025 et le port HTTP 1080, nous verrons donc nos emails à l'adresse [127.0.0.1:1080](http://127.0.0.1:1080)

Nous retournons maintenant dans notre classe de notification `ContactNotification`. et nous lui ajoutons un constructeur :
```
public function __construct
```



## Format MJML

[MJML](https://mjml.io/) est un language à balise de type HTML, créé par l'entreprise Mailjet pour écrire des emails responsives.

Pour envoyer l'email MJML se convertit en HTML qui peut donc être envoyé dans l'email.

Il existe un éditeur en ligne pour nous permettre de créer nos mises en page [ici](https://mjml.io/try-it-live)

Dans ce site je vous propose ce modèle pour notre formulaire de contact :
```
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>

        <mj-image src="https://via.placeholder.com/468x60?text=Company Logo"></mj-image>

        <mj-divider border-color="#F45E43"></mj-divider>

        <mj-section>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">Nom : </mj-text>
          </mj-column>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">{{ contact.lastname }}</mj-text>
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">Prénom : </mj-text>
          </mj-column>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">{{ contact.firstname }}</mj-text>
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">Téléphone : </mj-text>
          </mj-column>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">{{ contact.phone }}</mj-text>
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">Email : </mj-text>
          </mj-column>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">{{ contact.email }}</mj-text>
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text font-size="20px" color="#F45E43" font-family="helvetica">Message : </mj-text>
            <mj-text>{{ message }}</mj-text>
          </mj-column>
        </mj-section>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

ce qui nous génère le HTML suivant :
```
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

  <head>
    <title></title>
    <!--[if !mso]><!-- -->
    <meta
    http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
      #outlook a {
        padding: 0;
      }

      body {
        margin: 0;
        padding: 0;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }

      table,
      td {
        border-collapse: collapse;
        mso-table-lspace: 0;
        mso-table-rspace: 0;
      }

      img {
        border: 0;
        height: auto;
        line-height: 100%;
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
      }

      p {
        display: block;
        margin: 13px 0;
      }
    </style>
    <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
              <o:AllowPNG/>
              <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
    <!--[if lte mso 11]>
            <style type="text/css">
              .mj-outlook-group-fix { width:100% !important; }
            </style>
            <![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"> <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    </style>
    <!--<![endif]-->
    <style type="text/css">
      @media only screen and(min-width:480px) {
        .mj-column-per-100 {
          width: 100% !important;
          max-width: 100%;
        }
        .mj-column-per-50 {
          width: 50% !important;
          max-width: 50%;
        }
      }
    </style>
    <style type="text/css">
      @media only screen and(max-width:480px) {
        table.mj-full-width-mobile {
          width: 100% !important;
        }
        td.mj-full-width-mobile {
          width: auto !important;
        }
      }
    </style>
  </head>

  <body>
    <div
      style="">
      <!--[if mso | IE]>
            <table
               align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
            >
              <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]-->
      <div style="margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td
                style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                <!--[if mso | IE]>
                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                
                        <tr>
                      
                            <td
                               class="" style="vertical-align:top;width:600px;"
                            >
                          <![endif]-->
                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                    <tr>
                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                          <tbody>
                            <tr>
                              <td style="width:550px;">
                                <img height="auto" src="https://via.placeholder.com/468x60?text=Company Logo" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="550"/>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size:0px;padding:10px 25px;word-break:break-word;">
                        <p style="border-top:solid 4px #F45E43;font-size:1;margin:0px auto;width:100%;"></p>
                        <!--[if mso | IE]>
                                <table
                                   align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 4px #F45E43;font-size:1;margin:0px auto;width:550px;" role="presentation" width="550px"
                                >
                                  <tr>
                                    <td style="height:0;line-height:0;">
                                      &nbsp;
                                    </td>
                                  </tr>
                                </table>
                              <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="font-size:0px;padding:20px 0;word-break:break-word;">
                        <!--[if mso | IE]>
                              <table
                                 align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                              >
                                <tr>
                                  <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                              <![endif]-->
                        <div style="margin:0px auto;max-width:600px;">
                          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                            <tbody>
                              <tr>
                                <td
                                  style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                                  <!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                  
                                          <tr>
                                        
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">Nom :</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">{{ contact.lastname }}</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                          </tr>
                                        
                                                    </table>
                                                  <![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]>
                                  </td>
                                </tr>
                              </table>
                              <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="font-size:0px;padding:20px 0;word-break:break-word;">
                        <!--[if mso | IE]>
                              <table
                                 align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                              >
                                <tr>
                                  <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                              <![endif]-->
                        <div style="margin:0px auto;max-width:600px;">
                          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                            <tbody>
                              <tr>
                                <td
                                  style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                                  <!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                  
                                          <tr>
                                        
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">Prénom :</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">{{ contact.firstname }}</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                          </tr>
                                        
                                                    </table>
                                                  <![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]>
                                  </td>
                                </tr>
                              </table>
                              <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="font-size:0px;padding:20px 0;word-break:break-word;">
                        <!--[if mso | IE]>
                              <table
                                 align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                              >
                                <tr>
                                  <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                              <![endif]-->
                        <div style="margin:0px auto;max-width:600px;">
                          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                            <tbody>
                              <tr>
                                <td
                                  style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                                  <!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                  
                                          <tr>
                                        
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">Téléphone :</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">{{ contact.phone }}</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                          </tr>
                                        
                                                    </table>
                                                  <![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]>
                                  </td>
                                </tr>
                              </table>
                              <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="font-size:0px;padding:20px 0;word-break:break-word;">
                        <!--[if mso | IE]>
                              <table
                                 align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                              >
                                <tr>
                                  <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                              <![endif]-->
                        <div style="margin:0px auto;max-width:600px;">
                          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                            <tbody>
                              <tr>
                                <td
                                  style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                                  <!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                  
                                          <tr>
                                        
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">Email :</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                              <td
                                                 class="" style="vertical-align:top;width:300px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">{{ contact.email }}</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                          </tr>
                                        
                                                    </table>
                                                  <![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]>
                                  </td>
                                </tr>
                              </table>
                              <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="font-size:0px;padding:20px 0;word-break:break-word;">
                        <!--[if mso | IE]>
                              <table
                                 align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                              >
                                <tr>
                                  <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                              <![endif]-->
                        <div style="margin:0px auto;max-width:600px;">
                          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                            <tbody>
                              <tr>
                                <td
                                  style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                                  <!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                  
                                          <tr>
                                        
                                              <td
                                                 class="" style="vertical-align:top;width:600px;"
                                              >
                                            <![endif]-->
                                  <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#F45E43;">Message :</div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">{{ message }}</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--[if mso | IE]>
                                              </td>
                                            
                                          </tr>
                                        
                                                    </table>
                                                  <![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]>
                                  </td>
                                </tr>
                              </table>
                              <![endif]-->
                      </td>
                    </tr>
                  </table>
                </div>
                <!--[if mso | IE]>
                            </td>
                          
                        </tr>
                      
                                  </table>
                                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!--[if mso | IE]>
                </td>
              </tr>
            </table>
            <![endif]-->
    </div>
  </body>

</html>
```

Maintenant avec ce HTML nous pouvons créer le modèle TWIG de cet email, pour cela nous allons créer un nouveau modèle dans le dossier `templates/email/contact.html.twig`
et y coller ce modèle HTML généré.

Maintenant modifions le `ContactNotification.php` pour prendre en charge ce modèle.

Dans la fonction index nous modifions la génération de l'email selon la [documentation de symfony](https://symfony.com/doc/current/mailer.html#twig-html-css)
```
$email = (new TemplatedEmail())
      ->from($contact->getEmail())
      ->to(new Address('admin@monsite.fr'))
      ->subject('Nouvelle demande de ' . $contact->getLastname())
      ->htmlTemplate('emails/contact.html.twig')
      ->context([
        'contact' => $contact,
      ]);
```

Sans oublier d'importer les classes utilisées
```
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
```

Le code complet devient
```
<?php
namespace App\Notification;

use App\Entity\Contact;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class ContactNotification {

  /**
   * @var MailerInterface
   */
  private $mailer;

  public function __construct(MailerInterface $mailer)
  {
    $this->mailer = $mailer;
  }
  
  /**
   * notify envoi un email avec les informations de l'entité Contact
   *
   * @param  Contact $contact
   * @return void
   */
  public function notify(Contact $contact)
  {
    $email = (new TemplatedEmail())
      ->from($contact->getEmail())
      ->to(new Address('admin@monsite.fr'))
      ->subject('Nouvelle demande de ' . $contact->getLastname())
      ->htmlTemplate('emails/contact.html.twig')
      ->context([
        'contact' => $contact,
      ]);

    $this->mailer->send($email);
  }

}
```


## Créer une entité article

```
symfony console make:entity Article
```

Puis remplissez les champs en suivant le guide.

Pour les types de données vous avez la possibilité de taper `?` pour avoir la liste des possibilités

```
Field type (enter ? to see all types) [string]:
 > ?

Main types
  * string
  * text
  * boolean
  * integer (or smallint, bigint)
  * float

Relationships / Associations
  * relation (a wizard 🧙 will help you build the relation)
  * ManyToOne
  * OneToMany
  * ManyToMany
  * OneToOne

Array/Object Types
  * array (or simple_array)
  * json
  * object
  * binary
  * blob

Date/Time Types
  * datetime (or datetime_immutable)
  * datetimetz (or datetimetz_immutable)
  * date (or date_immutable)
  * time (or time_immutable)
  * dateinterval

Other Types
  * decimal
  * guid
  * json_array
```

Outre les types de données classiques :
 - Chaine de caractères
 - texte long
 - booléen
 - entier
 - nombre à virgule
 - dates
 - ...

Nous avons également des types de relations qui correspondent aux différents types de relation entre les tables d'une base de donnée.

il existe 3 types de relations entre 2 tables :
 - 1 <-> 1 : pour un enregistrement dans la table A j'ai, au plus, un enregistrement dans la table B.
 - 1 <-> N (ou N <-> 1 ): pour un enregistrement dans la table A j'ai plusieurs enregistrements dans la table B.
 - N <-> N : pour plusieurs enregistrements dans la table A j'ai plusieurs enregistrements dans la table B.

Exemples :
 - 1 <-> 1 : pour chaque utilisateur dans la table des utilisateurs je n'ai qu'une seule adresse email dans la table des emails
 - 1 <-> N : Pour une catégorie de la table des catégories j'ai plusieurs articles dans la table des articles
 - N <-> N : Pour plusieurs recettes dans la table recette, j'ai plusieurs ingrédients de la table ingrédient

Pour la N <-> N on va avoir besoin d'une table intermédiaire

```
Recette          Ingredient
-------          -------------
id_recette       id_ingredient
  ^                       ^
  |                       |
  |  RecetteIngredient    |
  |  -----------------    |
  -->id_recette           |
     id_ingredient<--------
```

